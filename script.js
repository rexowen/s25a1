db.fruits.aggregate([
{$match: {onSale: true}},
{$count: "Total Number of Fruits onSale"}
]);

db.fruits.aggregate([
{$match: {stock: { $gt: 20 }}},
{$count: "Total Number of Fruits with stock of more than 20: "}
]);

db.fruits.aggregate([
{ $match: {onSale: true}},
{ $group: {_id: "$supplier_id", total: {$avg: "$price"} } },
]);

db.fruits.aggregate([
{ $group: {_id: "$supplier_id", total: {$max: "$price"} } },
]);

db.fruits.aggregate([
{ $group: {_id: "$supplier_id", total: {$min: "$price"} } },
]);